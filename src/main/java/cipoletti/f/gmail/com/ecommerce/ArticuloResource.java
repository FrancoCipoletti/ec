package cipoletti.f.gmail.com.ecommerce;


import cipoletti.f.gmail.com.ecommerce.model.Articulo;
import cipoletti.f.gmail.com.ecommerce.service.ArticuloService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/articulo")
public class ArticuloResource {
    private final ArticuloService articuloService;

    public ArticuloResource(ArticuloService articuloService) {
        this.articuloService = articuloService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Articulo>> getAllArticulos() {
        List<Articulo> articulos = articuloService.findAllArticulos();
        return new ResponseEntity<>(articulos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Articulo> getAllArticulosById(@PathVariable("id") Long id) {
        Articulo articulo = articuloService.findArticuloById(id);
        return new ResponseEntity<>(articulo, HttpStatus.OK);
    }

    @GetMapping("/activos")
    public ResponseEntity<List<Articulo>> getAllArticulosActivos() {
        List<Articulo> articulo = articuloService.findArticulosActivos();
        return new ResponseEntity<>(articulo, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Articulo> addArticulo(@RequestBody Articulo articulo){
        Articulo newArticulo = articuloService.addArticulo(articulo);
        return new ResponseEntity<>(newArticulo,HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Articulo> updateArticulo(@RequestBody Articulo articulo){
        Articulo updateArticulo = articuloService.updateArticulo(articulo);
        return new ResponseEntity<>(updateArticulo,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Articulo> deleteArticulo(@PathVariable("id") Long id){
        articuloService.deleteArticulo(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

