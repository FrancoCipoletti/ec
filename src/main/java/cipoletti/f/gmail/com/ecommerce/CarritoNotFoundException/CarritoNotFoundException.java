package cipoletti.f.gmail.com.ecommerce.CarritoNotFoundException;

public class CarritoNotFoundException extends RuntimeException{
    public CarritoNotFoundException(String message) {
        super(message);
    }
}
