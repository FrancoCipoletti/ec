package cipoletti.f.gmail.com.ecommerce;


import cipoletti.f.gmail.com.ecommerce.model.Carrito;
import cipoletti.f.gmail.com.ecommerce.service.CarritoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/carrito")
public class CarritoResource {
    private final CarritoService CarritoService;

    public CarritoResource(CarritoService CarritoService) {
        this.CarritoService = CarritoService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Carrito>> getAllCarritos() {
        List<Carrito> Carritos = CarritoService.findAllCarritos();
        return new ResponseEntity<>(Carritos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Carrito> getAllCarritosById(@PathVariable("id") Long id) {
        Carrito Carrito = CarritoService.findCarritoById(id);
        return new ResponseEntity<>(Carrito, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Carrito> addCarrito(@RequestBody Carrito Carrito){
        Carrito newCarrito = CarritoService.addCarrito(Carrito);
        return new ResponseEntity<>(newCarrito,HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Carrito> updateCarrito(@RequestBody Carrito Carrito){
        Carrito updateCarrito = CarritoService.updateCarrito(Carrito);
        return new ResponseEntity<>(updateCarrito,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Carrito> deleteCarrito(@PathVariable("id") Long id){
        CarritoService.deleteCarrito(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}

