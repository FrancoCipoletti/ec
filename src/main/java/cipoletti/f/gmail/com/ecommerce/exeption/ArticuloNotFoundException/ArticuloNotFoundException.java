package cipoletti.f.gmail.com.ecommerce.exeption.ArticuloNotFoundException;

public class ArticuloNotFoundException extends RuntimeException{
    public ArticuloNotFoundException(String message) {
        super(message);
    }
}
