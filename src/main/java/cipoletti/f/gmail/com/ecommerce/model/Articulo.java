package cipoletti.f.gmail.com.ecommerce.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Articulo implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @Column(name="descripcion",nullable = false)
    private String descripcion;
    @Column(name="nombre",nullable = false)
    private String nombre;
    @Column(name="activo",nullable = false)
    private Boolean activo ;
    @Column(name="imagen",nullable = true)
    private String imagen;
    @Column(name="UUID",nullable = false)
    private String UUID;
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="idCarrito",referencedColumnName="id")
    private Carrito carrito;

    public Articulo(){}

    public Articulo(Long idArticulo, String descripcion, String nombre, Boolean activo, String imagen, String UUID) {
        this.id = idArticulo;
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.activo = activo;
        this.imagen = imagen;
        this.UUID = UUID;
    }


    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "Articulo{" +
                "id=" + id +
                ", descripcion='" + descripcion + '\'' +
                ", nombre='" + nombre + '\'' +
                ", activo=" + activo +
                ", imagen='" + imagen + '\'' +
                ", UUID='" + UUID + '\'' +
                '}';
    }
}
