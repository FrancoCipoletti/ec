package cipoletti.f.gmail.com.ecommerce.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
public class Carrito implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @OneToMany(cascade= CascadeType.MERGE)
    @JoinColumn(name="idArticulo",referencedColumnName="id")
    private List<Articulo> articulos;

    //private Usuario usuario;

    // Esta propiedad indica si el usuario presiono en comprar
    private Boolean comprado;
    private String UUID;
    public Carrito(){}

    public Carrito(Long idCarrito, List<Articulo> articulos, Boolean comprado) {
        this.id = idCarrito;
        this.articulos = articulos;
        //this.usuario = usuario;
        this.comprado = comprado;
    }

    public Long getIdCarrito() {
        return id;
    }

    public void setIdCarrito(Long idCarrito) {
        this.id = idCarrito;
    }

    public List<Articulo> getArticulos() {
        return articulos;

    }
    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getUUID() {
        return UUID;
    }

    public void setArticulos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

/*    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }*/

    public Boolean getComprado() {
        return comprado;
    }

    public void setComprado(Boolean comprado) {
        this.comprado = comprado;
    }
}
