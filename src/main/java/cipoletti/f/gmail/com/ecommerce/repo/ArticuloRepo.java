package cipoletti.f.gmail.com.ecommerce.repo;

import cipoletti.f.gmail.com.ecommerce.model.Articulo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import java.util.Optional;

public interface ArticuloRepo extends JpaRepository<Articulo,Long> {

    void deleteArticuloById(Long id);

    Optional<Articulo> findArticuloById(Long id);

    @Query(value = "SELECT * FROM articulo a WHERE a.activo=true", nativeQuery = true)
    List<Articulo> findArticulosActivos( );
}
