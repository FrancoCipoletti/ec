package cipoletti.f.gmail.com.ecommerce.repo;


import cipoletti.f.gmail.com.ecommerce.model.Carrito;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CarritoRepo extends JpaRepository<Carrito,Long> {

    void deleteCarritoById(Long id);

    Optional<Carrito> findCarritoById(Long id);

}
