package cipoletti.f.gmail.com.ecommerce.service;

import cipoletti.f.gmail.com.ecommerce.exeption.ArticuloNotFoundException.ArticuloNotFoundException;
import cipoletti.f.gmail.com.ecommerce.model.Articulo;
import cipoletti.f.gmail.com.ecommerce.repo.ArticuloRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

@Service
public class ArticuloService {
    private final ArticuloRepo articuloRepo;

    @Autowired
    public ArticuloService(ArticuloRepo articuloRepo) {
        this.articuloRepo = articuloRepo;
    }

    public Articulo addArticulo (Articulo articulo){
        articulo.setUUID(UUID.randomUUID().toString());
        return articuloRepo.save(articulo);
    }

    public List<Articulo> findAllArticulos(){
        return articuloRepo.findAll();
    }

    public Articulo updateArticulo(Articulo articulo){
        return articuloRepo.save(articulo);
    }

    public Articulo findArticuloById(Long id){
        return articuloRepo.findArticuloById(id).orElseThrow(()-> new ArticuloNotFoundException("Articulo con id "+ id + "no existe."));
    }

    public List<Articulo> findArticulosActivos(){
        return articuloRepo.findArticulosActivos();
    }

    public void deleteArticulo(Long id){
        articuloRepo.deleteArticuloById(id);

    }
}
