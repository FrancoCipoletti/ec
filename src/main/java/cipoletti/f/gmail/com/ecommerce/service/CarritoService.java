package cipoletti.f.gmail.com.ecommerce.service;

import cipoletti.f.gmail.com.ecommerce.CarritoNotFoundException.CarritoNotFoundException;
import cipoletti.f.gmail.com.ecommerce.model.Carrito;
import cipoletti.f.gmail.com.ecommerce.repo.CarritoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class CarritoService {
    private final CarritoRepo CarritoRepo;

    @Autowired
    public CarritoService(CarritoRepo CarritoRepo) {
        this.CarritoRepo = CarritoRepo;
    }

    public Carrito addCarrito (Carrito Carrito){
        Carrito.setUUID(UUID.randomUUID().toString());
        return CarritoRepo.save(Carrito);

    }

    public List<Carrito> findAllCarritos(){
        return CarritoRepo.findAll();
    }

    public Carrito updateCarrito(Carrito Carrito){
        return CarritoRepo.save(Carrito);
    }

    public Carrito findCarritoById(Long id){
        return CarritoRepo.findCarritoById(id).orElseThrow(()-> new CarritoNotFoundException("Carrito con id "+ id + "no existe."));
    }
    public void deleteCarrito(Long id){
        CarritoRepo.deleteCarritoById(id);

    }
}
